const fs = require('fs');
const path = require('path');
const usersFilePath = path.join(__dirname, 'users.json');

function loadUsers() {
    try {
        const data = fs.readFileSync(usersFilePath, 'utf8');
        return JSON.parse(data);
    } catch (error) {
        console.error('Error loading users:', error.message);
        return [];
    }
}

function saveUsers(users) {
    fs.writeFileSync(usersFilePath, JSON.stringify(users, null, 2));
}

module.exports = {
    loadUsers,
    saveUsers,
};