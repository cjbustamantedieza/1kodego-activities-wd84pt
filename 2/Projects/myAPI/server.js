// Import the necessary modules
const http = require('http');
const fs = require('fs');
const path = require('path');
const { loadUsers } = require('./utils/user-utils');

const server = http.createServer((req, res) => {
    if (req.method === 'POST') {
        if (req.url === '/register') {
            require('./routes/register')(req, res);
        } else if (req.url === '/login') {
            require('./routes/login')(req, res);
        } else {
            res.writeHead(404, { 'Content-Type': 'application/json' });
            res.end(JSON.stringify({ message: 'Not Found' }));
        }
    }
    else if (req.method === 'GET') {
        if (req.url === '/users_lists') {
            const users = loadUsers();
            if (users.length === 0) {
                res.writeHead(404, { 'Content-Type': 'application/json' });
                res.end(JSON.stringify({ message: 'No users found' }));
            } else {
                res.writeHead(200, { 'Content-Type': 'application/json' });
                res.end(JSON.stringify({ users }));
            }
        }
        else if (req.url === '/login') {
            fs.readFile(path.join(__dirname, 'public', 'index.html'), 'utf8', (err, data) => {
                if (err) {
                    res.writeHead(500, { 'Content-Type': 'text/plain' });
                    res.end('Internal Server Error');
                } else {
                    res.writeHead(200, { 'Content-Type': 'text/html' });
                    res.end(data);
                }
            });
        }
        else if (req.url === '/style.css') {
            fs.readFile(path.join(__dirname, 'public', 'style.css'), 'utf8', (err, data) => {
                if (err) {
                    res.writeHead(500, { 'Content-Type': 'text/plain' });
                    res.end('Internal Server Error');
                } else {
                    res.writeHead(200, { 'Content-Type': 'text/css' });
                    res.end(data);
                }
            });
        }
        else if (req.url === '/login-frontend.js') {
            fs.readFile(path.join(__dirname, 'public', 'login-frontend.js'), 'utf8', (err, data) => {
                if (err) {
                    res.writeHead(500, { 'Content-Type': 'text/plain' });
                    res.end('Internal Server Error');
                } else {
                    res.writeHead(200, { 'Content-Type': 'application/javascript' });
                    res.end(data);
                }
            });
        }
        else if (req.url === '/dashboard.html') {
            fs.readFile(path.join(__dirname, 'public', 'dashboard.html'), 'utf8', (err, data) => {
                if (err) {
                    res.writeHead(500, { 'Content-Type': 'text/plain' });
                    res.end('Internal Server Error');
                } else {
                    res.writeHead(200, { 'Content-Type': 'text/html' });
                    res.end(data);
                }
            });
        }
        else {
            res.writeHead(404, { 'Content-Type': 'application/json' });
            res.end(JSON.stringify({ message: 'Not Found' }));
        }
    }
    else {
        res.writeHead(405, { 'Content-Type': 'application/json' });
        res.end(JSON.stringify({ message: 'Method Not Allowed' }));
    }
});
server.listen(3000, () => {
    console.log('Server is running on port 3000');
});
