const { loadUsers, saveUsers } = require('../utils/user-utils');


function handleRegister(req, res) {
    let body = '';
    req.on('data', (chunk) => {
        body += chunk.toString();
    });

    req.on('end', () => {
        const { username, password } = JSON.parse(body);

        if (!username || !password) {
            res.writeHead(400, { 'Content-Type': 'application/json' });
            res.end(JSON.stringify({ message: 'Username and password are required' }));
            return;
        } else {
            const users = loadUsers();
            if (users.find(user => user.username === username)) {
                res.writeHead(409, { 'Content-Type': 'application/json' });
                res.end(JSON.stringify({ message: 'Username already exists' }));
            } else {
                users.push({ username, password });
                saveUsers(users);
                res.writeHead(201, { 'Content-Type': 'application/json' });
                res.end(JSON.stringify({ message: 'User registered successfully' }));
                return;
            }
        }
    });
}

module.exports = handleRegister;