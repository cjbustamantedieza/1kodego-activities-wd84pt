const fs = require('fs');
const path = require('path');
const userFilePath = path.join(__dirname, '../database/userData.json');


// To load the users
function loadUsers() {
    try {
        const data = fs.readFileSync(userFilePath, 'utf8');
        return JSON.parse(data);
    } catch (error) {
        console.log('Error loading users!', error.message)
        return [];
    }
}

// Push users
function saveUser(user) {
    try {
        fs.writeFileSync(userFilePath, JSON.stringify(user, null, 3));
    } catch (error) {
        console.error('Error saving user:', error);
    }
}









module.exports = {
    loadUsers,
    saveUser
}