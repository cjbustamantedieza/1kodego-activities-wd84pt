const { loadUsers, saveUser } = require('../data-access/user-access');


function userLogin(req, res) {
    let body = '';
    req.on('data', (chunk) => {
        body += chunk.toString();
    });
    req.on('end', () => {
        const { username, password } = JSON.parse(body);
        if (!username || !password) {
            res.writeHead(400, { 'Content-Type': 'application/json' });
            res.end(JSON.stringify({ message: 'Username and password are required!' }));
        } else {
            const users = loadUsers(); // Ensure this function is defined and correct.
            const user = users.find((user) => user.username === username);
            if (!user) {
                res.writeHead(401, { 'Content-Type': 'application/json' });
                res.end(JSON.stringify({ message: 'Account doesn\'t exist' }));
            } else if (user.password !== password) {
                res.writeHead(401, { 'Content-Type': 'application/json' });
                res.end(JSON.stringify({ message: 'Invalid password' }));
            } else {
                res.writeHead(200, { 'Content-Type': 'application/json' });
                res.end(JSON.stringify({ message: 'Login Successful' }));
            }
        }
    });
}


module.exports = {
    userLogin
}