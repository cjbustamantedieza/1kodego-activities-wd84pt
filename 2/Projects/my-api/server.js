const http = require('http');
const { userLogin } = require('./controller/login.js');
const { registerUser } = require('./controller/register.js');
const {
    addProduct,
    getSingleProduct,
    getAllProducts,
    deleteProduct,
    archiveProduct,
    updateProduct,
} = require('./controller/products.js');

const { 
    handleAsyncRequest,
    jsonResponse,
    errorResponse,
    notFound,
    methodNotAllowed
} = require('./utils/error-handler.js')


const port = 8080;

const server = http.createServer((req, res) => {
    handleRequest(req, res);
});

function handleRequest(req, res) {
//////////////////////////////POST
    if (req.method === 'POST') {
                            //REGGISTER A USER//////////////////////////////
        if (req.url === '/api/register') {
            return handleAsyncRequest(registerUser, req, res);
        }
                            //USER LOGIN//////////////////////////////
        if (req.url === '/api/login') {
            return handleAsyncRequest(userLogin, req, res);
        }
                            //ADD A PRODUCT//////////////////////////////
        if (req.url === '/api/add-product') {
            return handleAsyncRequest(addProduct, req, res);
        }
        notFound(res);
//////////////////////////////GET
    } else if (req.method === 'GET') {
                            //TEST//////////////////////////////
        if (req.url === '/api/data') {
            jsonResponse(res, { message: 'This is a GET request' });
                            //GET SINGLE PRODUCT BY ID//////////////////////////////
        } else if (req.url.startsWith('/api/get-single-product/')) {
            const productId = req.url.split('/').pop();
            return handleAsyncRequest(getSingleProduct, req, res, productId);
                            //GET ALL PRODUCTS//////////////////////////////
        } else if (req.url === '/api/products') {
            return handleAsyncRequest(getAllProducts, req, res);
                            //NO OBJECT//////////////////////////////
        } else {
            notFound(res);
        }
//////////////////////////////DELETE
    } else if (req.method === 'DELETE') {
        if (req.url.startsWith('/api/products/')) {
            const productId = req.url.split('/').pop();
            return handleAsyncRequest(deleteProduct, req, res, productId);
        } else {
            notFound(res);
        }
//////////////////////////////PUT
    } else if (req.method === 'PUT') {
                            //ARCHIVE A PRODUCT//////////////////////////////
        if (req.url.startsWith('/api/products/archive-product/')) {
            const productId = req.url.split('/').pop();
            return handleAsyncRequest(archiveProduct, req, res, productId);
                            //UPDATE A PRODUCT//////////////////////////////
        } else if (req.url.startsWith('/api/products/update-product/')) {
            const productId = req.url.split('/').pop();
            return handleAsyncRequest(updateProduct, req, res, productId);
        } else {
            notFound(res);
        }
    } else {
        methodNotAllowed(res);
    }
}



server.listen(port, () => {
    console.log(`Server is running on http://localhost:${port}`);
});
