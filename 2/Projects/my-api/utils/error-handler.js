async function handleAsyncRequest(handler, req, res, ...params) {
    try {
        await handler(req, res, ...params);
    } catch (error) {
        errorResponse(res, 500, 'Internal Server Error');
    }
}

function jsonResponse(res, data, statusCode = 200) {
    res.writeHead(statusCode, { 'Content-Type': 'application/json' });
    res.end(JSON.stringify(data));
}

function errorResponse(res, statusCode, message) {
    res.writeHead(statusCode, { 'Content-Type': 'application/json' });
    res.end(JSON.stringify({ error: message }));
}

function notFound(res) {
    errorResponse(res, 404, 'Not Found');
}

function methodNotAllowed(res) {
    sendErrorResponse(res, 405, 'Method Not Allowed');
}

module.exports = {
    handleAsyncRequest,
    jsonResponse,
    errorResponse,
    notFound,
    methodNotAllowed
}