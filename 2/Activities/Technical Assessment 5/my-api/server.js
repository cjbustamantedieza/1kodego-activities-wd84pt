const http = require('http');
const { registerUser } = require('./controller/register');
const { userLogin } = require('./controller/login');
const {
    addProduct,
    getSingleProduct,
    getAllProducts,
    deleteProduct,
    archiveProduct,
    updateProduct,
} = require('./controller/products');

const server = http.createServer((req, res) => {
    try {
        /////////////////////////////////////////////////
        // Middleware to parse JSON data from the request
        let body = '';
        req.on('data', (chunk) => {
            body += chunk.toString();
        });

        req.on('end', () => {
            req.body = body;
            handleRequest(req, res);
        });
        /////////////////////////////////////////////////
    } catch (error) {
        console.error('Error:', error);
        res.writeHead(500, { 'Content-Type': 'application/json' });
        res.end(JSON.stringify({ error: 'Internal Server Error' }));
    }
});

function handleRequest(req, res) {
    // Your existing routing and request handling logic goes here
    if (req.method === 'POST') {
        if (req.url === '/api/register') {
            registerUser(req, res);
        } else if (req.url === '/api/login') {
            userLogin(req, res);
        } else if (req.url === '/api/add-product') {
            addProduct(req, res);
        } else {
            sendNotFoundResponse(res);
        }
    } else if (req.method === 'GET') {
        if (req.url === '/api/data') {
            // Handle GET /api/data
            res.writeHead(200, { 'Content-Type': 'application/json' });
            const responseData = { message: 'This is a GET request' };
            res.end(JSON.stringify(responseData));
        } else if (req.url.startsWith('/api/get-single-product')) {
            // Handle GET /api/get-single-product
            const productId = req.url.split('/').pop();
            req.params = { id: productId };
            getSingleProduct(req, res);
        } else if (req.url === '/api/products') {
            // Handle GET /api/products
            getAllProducts(req, res);
        } else {
            sendNotFoundResponse(res);
        }
    } else if (req.method === 'DELETE') {
        if (req.url.startsWith('/api/products/')) {
            const productId = req.url.split('/').pop();
            deleteProduct(req, res, productId);
        } else {
            sendNotFoundResponse(res);
        }
    } else if (req.method === 'PUT') {
        if (req.url.startsWith('/api/products/archive-product')) {
            const productId = req.url.split('/').pop();
            archiveProduct(req, res, productId);
        } else if (req.url.startsWith('/api/products/update-product/')) {
            const productId = req.url.split('/').pop();
            updateProduct(req, res, productId);
        } else {
            sendNotFoundResponse(res);
        }
    } else {
        sendMethodNotAllowedResponse(res);
    }
}

function sendNotFoundResponse(res) {
    res.writeHead(404, { 'Content-Type': 'text/plain' });
    res.end('Not Found');
}

function sendMethodNotAllowedResponse(res) {
    res.writeHead(405, { 'Content-Type': 'text/plain' });
    res.end('Method Not Allowed');
}

const port = 8080;
server.listen(port, () => {
    console.log(`Server is running on http://localhost:${port}`);
});
