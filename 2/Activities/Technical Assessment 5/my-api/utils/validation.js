function validateProduct(product) {
    const { name, brand, price, category } = product;
    return typeof name === 'string' && name !== '' &&
        typeof brand === 'string' && brand !== '' &&
        typeof price === 'number' && price >= 0 &&
        typeof category === 'string' && category !== '';
}

function validateUserRegistration(user) {
    const {username, email, password, isAdmin} = user;
    return typeof username === 'string' && username !== '' &&
    typeof password === 'string' && /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email) &&
    typeof isAdmin === 'boolean';
}


module.exports = {
    validateProduct,
    validateUserRegistration
}