const { loadUsers } = require('../data-access/user-access');

function userLogin(req, res) {
    let body = '';
    req.on('data', (chunk) => {
        body += chunk.toString();
    });
    req.on('end', () => {
        const { username, password } = JSON.parse(body);
        if (!username || !password) {
            res.writeHead(404, { 'Content-Type': 'application/json' });
            res.end(JSON.stringify({ message: 'Username and password are required!' }));
        }
        else {
            const users = loadUsers();
            const user = users.find(user => user.username === username);
            if (!user || user.password !== password) {
                res.writeHead(200, { 'Content-Type': 'application/json' });
                res.end(JSON.stringify({ message: 'Login Successfull' }));

            }
            else {
                res.writeHead(200, { 'Content-Type': 'application/json' });
                res.end(JSON.stringify({ message: 'Login Successful' }));
            }
        }
    });
}



module.exports = {
    userLogin
}