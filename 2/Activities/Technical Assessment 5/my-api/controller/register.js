const { loadUsers, saveUser } = require('../data-access/user-access');
const { validateUserRegistration } = require('../utils/validation');
const defaultHeaders = { "Content-Type": "application/json" };




function registerUser(req, res) {
    try {
        let body = '';
        req.on('data', (chunk) => {
            body += chunk.toString();
        });
        req.on('end', () => {
            try {
                const userData = JSON.parse(body);
                if (!validateUserRegistration(userData)) {
                    res.writeHead(400, { 'Content-Type': 'application/json' });
                    res.end(JSON.stringify({ error: 'Invalid user registration input' }));
                    return;
                }
                const users = loadUsers();
                if (users.find((user) => user.username === userData.username)) {
                    res.writeHead(409, defaultHeaders);
                    res.end(JSON.stringify({ message: 'Username already exists' }));
                } else {
                    users.push(userData);
                    saveUser(users);
                    res.writeHead(201, defaultHeaders);
                    res.end(JSON.stringify({ message: 'User registered successfully' }));
                }
            } catch (error) {
                res.writeHead(404, defaultHeaders);
                res.end(JSON.stringify({ message: 'Error' }));
            }
        });
    } catch (error) {
        res.writeHead(404, defaultHeaders);
        res.end(JSON.stringify({ message: 'Error' }));
    }
}




module.exports = {
    registerUser
};

