const fs = require('fs');
const path = require('path');
const productFilePath = path.join(__dirname, '../database/productData.json');
const archivedProducts = path.join(__dirname, '../database/archivedData.json');


// Product file reader
function loadProducts() {
    try {
        const data = fs.readFile(productFilePath, 'utf8');
        return JSON.parse(data);
    } catch (error) {
        console.log('Error loading products:', error.message);
        return [];
    };
};

function saveProducts(products) {
    try {
        fs.writeFile(productFilePath, JSON.stringify(products, null, 2))
    } catch (error) {
        console.log('Error saving products:', error.message);
    };
};


// Archived file reader
function loadArchivedProducts() {
    try {
        const data = fs.readFileSync(archivedProducts, 'utf8');
        return JSON.parse(data);
    } catch (error) {
        console.log('Error loading archived products:', error.message);
        return [];
    }
}

function saveArchivedProducts(archivedItem) {
    try {
        fs.writeFileSync(archivedProducts, JSON.stringify(archivedItem, null, 2));
    } catch (error) { // Include the error parameter here
        console.log('Error saving archived products:', error.message);
    }
}





module.exports = {
    loadProducts,
    saveProducts,
    loadArchivedProducts,
    saveArchivedProducts
}