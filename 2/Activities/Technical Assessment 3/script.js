// Excercise 1

function createCar(brand, model, year, color, mileAge, price) {
    return {
        brand,
        model,
        year,
        color,
        mileAge,
        price,
    };
}
const cars = [];
cars.push(createCar('Ford', 'Raptor', 2019, 'red', '2000km', 33500));
cars.push(createCar('Ford1', 'Raptor1', 2019, 'black', '2100km', 31000));
cars.push(createCar('Ford2', 'Raptor2', 2020, 'white', '2200km', 34550));
console.log(cars);
function totalPrice(cars) {
    let total = 0;
    for (const car of cars) {
        total += car.price;
    }
    return total / cars.length;
}
console.log(totalPrice(cars));





// Excercise 2
function Book(title, author, pages, year, genre) {
    this.title = title;
    this.author = author;
    this.pages = pages;
    this.year = year;
    this.genre = genre;
  }
  const book1 = new Book("The Great Gatsby", "F. Scott Fitzgerald", 180, 1925, "Fiction");
  const book2 = new Book("To Kill a Mockingbird", "Harper Lee", 281, 1960, "Fiction");
  const book3 = new Book("The Hobbit", "J.R.R. Tolkien", 310, 1937, "Fantasy");
  function getBooksByGenre(books, genreToFilter) {
    const filteredBooks = [];
    for (const book of books) {
      if (book.genre === genreToFilter) {
        filteredBooks.push(book.title);
      }
    }
    return filteredBooks;
  }
  const allBooks = [book1, book2, book3];
  const fictionBooks = getBooksByGenre(allBooks, "Fiction");
  console.log("Fiction Books:", fictionBooks);
  






//   Excercise 3
const cart = [
    { name: 'Shirt', price: 20, quantity: 2 },
    { name: 'Pants', price: 30, quantity: 5 },
    { name: 'Socks', price: 5, quantity: 7 },
    { name: 'Hat', price: 40, quantity: 8 },
    { name: 'Shoes', price: 70, quantity: 10}
];

// 1. Write a function that takes the shopping cart array as a argument/parameter and returns the total cost of all the items in the cart.

function calculateTotalCost(cart) {
    let totalCost = 0;
  
    for (const item of cart) {
      totalCost += item.price * item.quantity;
    }
  
    return totalCost;
  }

  const totalCost = calculateTotalCost(cart);
console.log('Total Cost:', totalCost);

// 2.	Write a function that takes the shopping cart array as a argument/parameter and returns an array of all items that has more than five stocks and its price is greater than or equal to 20.
function filterCart(cart) {
    return cart.filter(item => item.quantity > 5 && item.price >= 20);
  }
  const filteredItems = filterCart(cart);
console.log('Items with more than 5 stocks and price >= 20:');
console.log(filteredItems);

// 3. Write a function that takes the shopping cart array as a parameter and returns an array of all the items whose price is greater than the supplied amount (specified as a parameter to the function).

function filterCartByPrice(cart, priceThreshold) {
    return cart.filter(item => item.price > priceThreshold);
  }
  const priceThreshold = 25;

const itemsAboveThreshold = filterCartByPrice(cart, priceThreshold);
console.log(`Items with price greater than $${priceThreshold}:`);
console.log(itemsAboveThreshold);





// Excercise 4
const students = [
    { name: 'Khelly', grades: [78, 92, 85, 88] },
    { name: 'Jared', grades: [90, 86, 94, 89] },
    { name: 'Aloi', grades: [72, 84, 80, 76] },
    { name: 'Alvin', grades: [68, 72, 65, 70] }
];
// Write a function that takes this array as a parameter and returns an array of objects representing the average grade for each student. Each object in the returned array should have the following properties: name(string)  averageGrade (number, rounded to two decimal places)
function calculateAverageGrades(students) {
    return students.map(student => {
      const sum = student.grades.reduce((total, grade) => total + grade, 0);
      const averageGrade = sum / student.grades.length;
      return {
        name: student.name,
        averageGrade: parseFloat(averageGrade.toFixed(2)) // Round to 2 decimal places
      };
    });
  }
  const averageGrades = calculateAverageGrades(students);
  console.log(averageGrades);