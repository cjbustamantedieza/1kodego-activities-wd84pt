function Car(brand, model, year, color, mileAge, price) {
    this.brand = brand;
    this.model = model;
    this.year = year;
    this.color = color;
    this.milAge = mileAge;
    this.price = price;
}

const honda = new Car('Honda', 'Civic', 2018, 'Silver', 100000, 300000);
const toyota = new Car('Toyota', 'Corolla', 1999, 'Red', 300000, 150000);
const ford = new Car('Ford', 'Mustang', 2020, 'White', 20000, 1500000);

const carCollection = [honda, toyota, ford]

function calculateAveragePrice(cars) {
    const sum = cars.map(car => car.price).reduce((accumulator, currentValue) => accumulator + currentValue, 0);
    console.log(`The average of total price of cars is: ${sum / carCollection.length}`)
}

calculateAveragePrice(carCollection);


// ----Exercise 2

function Book(title, author, pages, year, genre) {
    this.title = title;
    this.author = author;
    this.pages = pages;
    this.year = year;
    this.genre = genre;
}

const harryPotter = new Book('Harry Potter: Chamber of Secrets', 'J.K Rowling', 3000, 1998, 'Fantasy');
const hungerGames = new Book('Hunger Games: Catching Fire', 'Suzanne Collins', 3500, 2009, 'Fiction');
const lordOfTheRings = new Book('Lord of The Rings: The Treason of Isengard', 'J.R.R Tolkien', 3000, 1989, 'Fantasy');

const bookCollection = [harryPotter, hungerGames, theLordOfTheRings];

function filterBooksByGenre(books, genre) {
    let foundBooks = [];
    books.map(book => book.genre === genre ? foundBooks.push(books.title) : null);
    console.log(foundBooks.length === 0 ? 'No Book Found')
}


