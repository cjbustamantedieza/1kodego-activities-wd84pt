let students = new Array("john", "Mia", "Lee");

let addStudents = (names) => {
    let count = 0;
    for (let name of names) {
        students.push(name);
        count++; 
        console.log(`${name} is added to the student list`);
    }
    return count;
}

let addedCount = addStudents(["John", "Emily", "Alex", "Jason"]);
console.log(`Total number of students added to the list: ${addedCount}`);
