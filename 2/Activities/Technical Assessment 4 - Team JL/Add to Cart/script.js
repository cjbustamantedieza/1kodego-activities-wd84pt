document.addEventListener('DOMContentLoaded', function() {
    const addToCartButtons = document.querySelectorAll('.add-to-cart');
    const cartItemsElement = document.getElementById('cart-items');
    const cartTotalElement = document.getElementById('cart-total');

    let cart = [];

    function addToCart(name, price, imgSrc) {
        const existingItem = cart.find(item => item.name === name);
        if (existingItem) {
            existingItem.quantity++;
        } else {
            cart.push({ name, price, imgSrc, quantity: 1 });
        }
        updateCartDisplay();
    }

    function removeFromCart(index) {
        cart.splice(index, 1);
        updateCartDisplay();
    }

    function updateCartDisplay() {
            cartItemsElement.innerHTML = '';
            let total = 0;

        cart.forEach((item, index) => {
            total += item.price * item.quantity;
            const li = document.createElement('li');
            li.className = 'row d-flex flex-row justify-content-between align-items-center mt-3';
            li.innerHTML = `
                    <div class="col-sm-3">
                        <img src="${item.imgSrc}" alt="${item.name}" style="width: 100px; height: 70px;">
                    </div>
                    <div class="col-sm-3"> 
                        <span>₱${item.price.toFixed(2)}</span> 
                    </div> 
                    <div class="col-sm-2">
                        <span>${item.quantity} pcs</span>
                    </div> `;
            const removeDiv = document.createElement('div');
            const removeButton = document.createElement('button');
            removeButton.textContent = 'Remove';
            removeDiv.className = 'col-sm-4';
            removeButton.className = 'allBtn';
            removeButton.addEventListener('click', () => removeFromCart(index));
            removeDiv.appendChild(removeButton);
            li.appendChild(removeDiv);
            cartItemsElement.appendChild(li);

            console.log(item.imgSrc);
        });

        cartTotalElement.textContent = total.toFixed(2);
    }

    addToCartButtons.forEach(button => {
        button.addEventListener('click', function() {
            const name = this.dataset.name;
            const price = parseFloat(this.dataset.price);
            const imgSrc = this.dataset.imgsrc;
            addToCart(name, price, imgSrc);
        });
    });
});
