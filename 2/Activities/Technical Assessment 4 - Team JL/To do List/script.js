
document.addEventListener('DOMContentLoaded', function() {
    const modalTaskInput = document.getElementById('modalTaskInput');
    const modalAddTaskBtn = document.getElementById('modalAddTaskBtn');
    const taskList = document.getElementById('taskList');
    const confirmDeleteBtn = document.getElementById('otherDelete');
    const deleteConfirmationModal = new bootstrap.Modal(document.getElementById('deleteConfirmationModal'));
    let targetToDelete;

    modalAddTaskBtn.addEventListener('click', addTask);

    modalTaskInput.addEventListener('keypress', function(event) {
        if (event.key === 'Enter') {
            modalAddTaskBtn.click();
        }
    });

    function addTask() {
        const taskText = modalTaskInput.value.trim();

        if (taskText !== '') {
            const tr = document.createElement('tr');    
            tr.innerHTML = `
                <td style="font-size: 20px;">${taskText}</td>
                <td class="status"><span class="badge text-bg-warning" style="font-size: 20px;">In Progress</span></td>
                <td>
                    <i class='deleteBtn bx bxs-trash' style="font-size: 30px;"></i>
                </td>
            `;
            taskList.appendChild(tr);
            modalTaskInput.value = '';

        }
    }

    taskList.addEventListener('click', function(event) {
        if (event.target.classList.contains('deleteBtn')) {
            deleteConfirmationModal.show();
            targetToDelete = event.target.parentElement.parentElement;
        }
    });

    confirmDeleteBtn.addEventListener('click', function() {
        targetToDelete.remove();
        deleteConfirmationModal.hide();
    });
});
