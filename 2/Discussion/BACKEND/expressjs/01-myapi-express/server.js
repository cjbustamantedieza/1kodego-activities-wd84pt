const express = require('express');
const app = express();
const PORT = 8080;


app.use(express.json());

const users = new Array(
   {
      id: 1,
      name: 'Juan'
   },
   {
      id: 2,
      name: 'Leo'
   },
   {
      id: 3,
      name: 'Max'
   },
   
)


app.post('/test', (req, res) => {
   res.send('Hello there');
});

app.get('/getAllUsers', (req, res) => {
   res.status(200).json(users)
});

app.get('/users/:userId', (req, res) => {
   const userId = Number(req.params.userId);
   const user = users.find(user => user.id === userId);
   if (user) {
      res.status(200).json(user);
   } else {
      res.status(400).json({
         status: false,
         message: 'User not found'
      });
   };
});

app.post('/register', (req, res) => {
   const {name, userName, password} = req.body;
   const user = { name, userName, password };
   const userExist = users.some(user => user.userName === userName)
   if (userExist) {
      res.status(400).json({
         status: false,
         message: 'User already exists'
      });
      return;

   }

   if (!name || !userName || !password) {
      res.status(404).json({
         status: false,
         message: 'Fields cannot be empty!'
      })
   } else {
      users.push(user);
      res.status(200).json({
         status: true,
         message: 'Success!',
         users
      })
   }
})



app.listen(PORT, () => {
   console.log(`Server is listening to http://localhost:${PORT}`);
});

