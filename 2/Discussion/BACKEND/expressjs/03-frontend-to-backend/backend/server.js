const express = require('express');
const cors = require('cors');
const fs = require('fs');
const path = require('path'); // Import the path module
const app = express();
const PORT = 8080;

app.use(express.json());
app.use(cors({
    origin: '*'
}));
const users = [];

const dataFilePath = path.join(__dirname, 'data.json');

app.post('/register', (req, res) => {
    const { name, userName, password } = req.body;
    const user = { name, userName, password };
    const userExist = users.some((user) => user.userName === userName);

    if (userExist) {
        res.status(400).json({
            status: false,
            message: 'User already exists',
        });
    } else if (!name || !userName || !password) {
        res.status(400).json({
            status: false,
            message: 'Fields cannot be empty!',
        });
    } else {
        users.push(user);
        fs.writeFile(dataFilePath, JSON.stringify(users), (err) => {
            if (err) {
                console.error(err);
            } else {
                console.log('User data saved to data.json');
            }
        });

        res.status(200).json(user);
    }
});

app.listen(PORT, () => {
    console.log(`Server is listening to http://localhost:${PORT}`);
});
