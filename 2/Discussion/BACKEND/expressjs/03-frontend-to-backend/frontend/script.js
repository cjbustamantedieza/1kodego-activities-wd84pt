// function fetchUsers() {
//     const request = new XMLHttpRequest();
//     request.open('GET', 'http://localhost:8080/users', true);

//     request.onload = () => {
//         if (request.status >= 200 && request.status < 300) {
//             var data = JSON.parse(request.responseText);
//             console.log(data);
//         } else {
//             console.error('Request failed: ', request.status);
//         }
//     }
//     request.onerror = () => {
//         console.error('Request failed');
//     }
//     request.send();
// }


const userContainer = document.getElementById('user-container');
const userTable = document.getElementById('user-table');
const addUserForm = document.getElementById('add-user-form');

function fetchUsers() {
    fetch('http://localhost:8080/users')
    .then(response => {
        if (response.ok) {
            return response.json();
        } else {
            throw new Error('Request failed', response.status);
        }
    })
    .then(users => {
        // Clear the current content of the table
        userTable.innerHTML = '';

        // Loop through the users and add them to the table
        users.forEach(user => {
            const newRow = userTable.insertRow();
            const cell1 = newRow.insertCell(0);
            const cell2 = newRow.insertCell(1);
            cell1.innerHTML = user.name;
            cell2.innerHTML = user.userName;
        });
    })
    .catch(error => {
        console.error('Request failed!', error.message);
    });
}


function showAddUserForm() {
    addUserForm.style.display = 'block';
}

function addUser() {
    const username = document.getElementById('username').value;
    const email = document.getElementById('email').value;
    const password = document.getElementById('password').value;

    fetch('http://localhost:8080/register', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({ name: username, userName: email, password }),
    })
        .then(response => {
            if (response.ok) {
                return response.json();
            } else {
                throw new Error('Request failed', response.status);
            }
        })
        .then(data => {

            const newRow = userTable.insertRow();
            const cell1 = newRow.insertCell(0);
            const cell2 = newRow.insertCell(1);
            cell1.innerHTML = data.name;
            cell2.innerHTML = data.userName;


            document.getElementById('username').value = '';
            document.getElementById('email').value = '';
            document.getElementById('password').value = '';
            addUserForm.style.display = 'none';
            // Add this line at the end of your script.js to fetch and display users on page load
            fetchUsers();

        })
        .catch(error => {
            console.error('Request failed!', error.message);
        });
}

