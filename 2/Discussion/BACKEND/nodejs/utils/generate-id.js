function  objId() {
    const timestamp = new Date().getTime().toString(16);
    const randomNum = Math.random().toString(16).substr(2, 6);
    return timestamp + randomNum;
} 


module.exports = {
    objId
}