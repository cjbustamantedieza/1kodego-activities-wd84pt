const { loadProducts,
    saveProducts,
    loadArchivedProducts,
    saveArchivedProducts
} = require('../data-access/product-access');
// const uuid = require('uuid');
const { objId, } = require('../utils/generate-id');
const {validateProduct} = require('../utils/validation');
const {defaultHeader} = {'Content-Type' : 'application.json'};
////////////////////////////////Add a product
function addProduct(req, res) {
    try {
        let body = '';
        req.on('data', (chunk) => {
            body += chunk.toString();
        });
        req.on('end', () => {
            try {
                const productData = JSON.parse(body);
                if (validateProduct(productData)) {
                    const newProduct = {
                        id: objId(),
                        archived: false,
                        ...productData
                    };
                    const products = loadProducts();
                    products.push(newProduct);
                    saveProducts(products);
                    res.writeHead(201, { 'Content-type': 'application/json' });
                    res.end(JSON.stringify({ message: 'Product added successfully', newProduct}));
                } else {
                    res.writeHead(400, { 'Content-type': 'application/json' });
                    res.end(JSON.stringify({ error: 'Invalid product data' }));
                }
            } catch (error) {
                res.writeHead(400, { 'Content-type': 'application/json' });
                res.end(JSON.stringify({ error: 'Invalid JSON data' }));
            }
        });
    } catch (error) {
        console.error('Error adding a product:', error);
        res.writeHead(404, { 'Content-type': 'application/json' });
        res.end(JSON.stringify({ error: 'Internal server error' }));
    }
}

//////////////////////////////// Get single product
function getSingleProduct(req, res) {
    const productId = req.params.id;
    const products = loadProducts();
    const product = products.find((item) => item.id === productId);
    if (!product) {
        res.writeHead(404, { 'Content-Type': 'application/json' });
        res.end(JSON.stringify({ error: 'Product not found' }));
    } else {
        res.writeHead(200, { 'Content-Type': 'application/json' });
        res.end(JSON.stringify(product));
    }
}

//////////////////////////////// Get all products
function getAllProducts(req, res) {
    try {
        const products = loadProducts();
    res.writeHead(200, { 'Content-Type': 'application/json' });
    res.end(JSON.stringify(products));
    } catch (error) {
        console.log(error)
        res.writeHead(404, defaultHeader);
        res.end(JSON.stringify({error : "Internal Server Error"}));
    }
    
}

//////////////////////////////// Delete a product
function deleteProduct(req, res, productId) {
    try {
        const products = loadProducts();
        const productIndex = products.findIndex((item) => item.id === productId);

        if (productIndex === -1) {
            res.writeHead(404, { 'Content-Type': 'application/json' });
            res.end(JSON.stringify({ error: 'Product not found' }));
        } else {
            const deletedProduct = products.splice(productIndex, 1)[0];
            saveProducts(products);
            res.writeHead(200, { 'Content-type': 'application/json' });
            res.end(JSON.stringify({ message: 'Product deleted successfully', deletedProduct }));
        }
    } catch (error) {
        console.error('Error deleting a product:', error);
        res.writeHead(500, { 'Content-type': 'application/json' });
        res.end(JSON.stringify({ error: 'Internal server error', details: error.message }));
    }
}


////////////////////////////////Archive a product
function archiveProduct(req, res, productId) {
    try {
        let products = loadProducts();
        const archivedProducts = loadArchivedProducts();
        const productIndex = products.findIndex((item) => item.id === productId);
        if (productIndex === -1) {
            res.writeHead(404, { 'Content-Type': 'application/json' });
            res.end(JSON.stringify({ error: 'Product not found' }));
        } else {
            const archivedProduct = { ...products[productIndex], archived: true };
            products.splice(productIndex, 1);
            saveProducts(products);
            archivedProducts.push(archivedProduct);
            saveArchivedProducts(archivedProducts);
            res.writeHead(200, { 'Content-type': 'application/json' });
            res.end(JSON.stringify({ message: 'Product archived successfully', archivedProduct }));
        }
    } catch (error) {
        console.error('Error archiving a product:', error);
        res.writeHead(500, { 'Content-type': 'application/json' });
        res.end(JSON.stringify({ error: 'Internal server Error' }));
    }
}

////////////////////////////////Update a product
function updateProduct(req, res, productId) {
    try {
        const products = loadProducts();
        const productIndex = products.findIndex((item) => item.id === productId);

        if (productIndex === -1) {
            res.writeHead(404, { 'Content-Type': 'application/json' });
            res.end(JSON.stringify({ error: 'Product not found' }));
            return;
        }
        const updatedProductData = JSON.parse(req.body);

        if (!validateProduct(updatedProductData)) {
            res.writeHead(400, { 'Content-Type': 'application/json' });
            res.end(JSON.stringify({ error: 'Invalid product data' }));
        }
        products[productIndex] = {      //
            ...products[productIndex], //ref//https://developer.mozilla.org/en-US/docs/  Web/JavaScript/Reference/Operators/Spread_syntax
            ...updatedProductData,    //
        };
        saveProducts(products);
        res.writeHead(200, { 'Content-Type': 'application/json' });
        res.end(JSON.stringify({ message: 'Product updated successfully', updatedProduct: products[productIndex] }));
    } catch (error) {
        console.error('Error updating a product:', error);
        res.writeHead(500, { 'Content-Type': 'application/json' });
        res.end(JSON.stringify({ error: 'Internal server error' }));
    }
}






module.exports = {
    addProduct,
    getSingleProduct,
    getAllProducts,
    deleteProduct,
    archiveProduct,
    updateProduct
};
